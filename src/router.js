import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'Inicio',
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/icons',
          name: 'icons',
          component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import(/* webpackChunkName: "demo" */ './views/UserProfile.vue')
        },
        {
          path: '/maps',
          name: 'maps',
          component: () => import(/* webpackChunkName: "demo" */ './views/Maps.vue')
        },
        {
          path: '/tables',
          name: 'tables',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
        },
        {
          path: '/categories',
          name: 'Categorias',
          component: () => import(/* webpackChunkName: "demo" */ './views/Categories/CategoryList.vue')
        },
        {
          path: '/categories/create',
          name: 'Nueva categoria',
          component: () => import(/* webpackChunkName: "demo" */ './views/Categories/CategoryForm.vue')
        },
        {
          path: '/products',
          name: 'Productos',
          component: () => import(/* webpackChunkName: "demo" */ './views/Products/ProductList.vue')
        },
        {
          path: '/products/create',
          name: 'Nuevo producto',
          component: () => import(/* webpackChunkName: "demo" */ './views/Products/ProductForm.vue')
        },
        {
          path: '/plans',
          name: 'Planes',
          component: () => import(/* webpackChunkName: "demo" */ './views/Plans/PlanList.vue')
        },
        {
          path: '/invoices',
          name: 'Facturas',
          component: () => import(/* webpackChunkName: "demo" */ './views/Invoices/InvoiceList.vue')
        }
      ]
    },
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import(/* webpackChunkName: "demo" */ './views/Login.vue')
        },
        {
          path: '/register',
          name: 'register',
          component: () => import(/* webpackChunkName: "demo" */ './views/Register.vue')
        }
      ]
    }
  ]
})
